package tr.com.turkcell.ttech.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tr.com.turkcell.ttech.service.CustomerService;
import tr.com.turkcell.ttech.tables.pojos.Customer;

@Component
@Path("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    
    @GET
    @Path("/")
    @Produces(APPLICATION_JSON)
    public List<Customer> list() {
        return customerService.list();
    }

    @GET
    @Path("/{id}")
    @Produces(APPLICATION_JSON)
    public Customer get(@PathParam("id") String id) {
        Customer customer = customerService.get(id);
        if (customer == null) {
            throw new WebApplicationException(Status.NOT_FOUND);
        }
        return customer;
    }

    @GET
    @Path("/search")
    @Produces(APPLICATION_JSON)
    public List<Customer> search(@QueryParam("msisdn") String msIsdn,
                                 @QueryParam("name") String name,
                                 @QueryParam("surname") String surname) {
        if ((msIsdn == null || msIsdn.trim().isEmpty()) &&
                (name == null || name.trim().isEmpty()) &&
                (surname == null || surname.trim().isEmpty())) {
            return Collections.emptyList();
        }
        return customerService.search(msIsdn, name, surname);
    }
}
