package tr.com.turkcell.ttech.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.springframework.stereotype.Component;

@Component
@Path("/status")
public class StatusController {

    @GET
    public String status() {
        return "ok";
    }
}
