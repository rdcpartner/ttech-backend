package tr.com.turkcell.ttech.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tr.com.turkcell.ttech.domain.ErrorBean;
import tr.com.turkcell.ttech.domain.OrderBean;
import tr.com.turkcell.ttech.service.OrderService;
import tr.com.turkcell.ttech.service.OtpService;

@Component
@Path("/otp")
public class OtpController {

    @Autowired
    private OtpService otpService;

    @Autowired
    private OrderService orderService;

    @POST
    @Path("/{customerId}")
    @Produces(APPLICATION_JSON)
    public Response send(@PathParam("customerId") String customerId) {
        OrderBean order = orderService.getByCustomerId(customerId);
        if (order != null) {
            String otpId = otpService.send(customerId);
            Map<String, String> map = new LinkedHashMap<>();
            map.put("otpId", otpId);
            return Response.ok(map).build();
        } else {
            throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
                    .entity(new ErrorBean("Müşteri sepeti mevcut değil."))
                .build());
        }
    }

    @POST
    @Path("/verify/{customerId}")
    @Produces(APPLICATION_JSON)
    public Response verfiy(@PathParam("customerId") String customerId) {
        otpService.verify(customerId);
        return Response.ok().build();
    }
}
