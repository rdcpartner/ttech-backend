package tr.com.turkcell.ttech.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tr.com.turkcell.ttech.service.ProductService;
import tr.com.turkcell.ttech.tables.pojos.Product;

@Component
@Path("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @POST
    @Path("/")
    @Consumes(APPLICATION_JSON)
    public Product create(Product product) {
        return productService.create(product);
    }

    @PUT
    @Path("/")
    @Consumes(APPLICATION_JSON)
    public Product update(Product product) {
        return productService.update(product);
    }
    
    @GET
    @Path("/")
    @Produces(APPLICATION_JSON)
    public List<Product> list() {
        return productService.list();
    }

    @GET
    @Path("/{id}")
    @Produces(APPLICATION_JSON)
    public Product get(@PathParam("id") String id) {
        return productService.get(id);
    }
}
