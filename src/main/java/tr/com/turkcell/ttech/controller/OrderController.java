package tr.com.turkcell.ttech.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.status;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tr.com.turkcell.ttech.domain.ErrorBean;
import tr.com.turkcell.ttech.domain.OrderBean;
import tr.com.turkcell.ttech.service.OrderService;
import tr.com.turkcell.ttech.tables.pojos.CustomerOrder;

@Component
@Path("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @POST
    @Path("/{customerId}")
    @Produces(APPLICATION_JSON)
    public Response create(@PathParam("customerId") String customerId) {
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setCustomerId(customerId);
        OrderBean existingOrder = orderService.getByCustomerId(customerId);
        if ( existingOrder != null && ! existingOrder.isVerified() ) {
            return Response.status(Status.BAD_REQUEST)
                            .entity(new ErrorBean("Müşteri mevcut bir sepeti bulunmakta. " +
                                        "Yeni bir sipariş oluşturmak için, mevcut siparişi sonlandırnız veya iptal ediniz."))
                        .build();
        }
        CustomerOrder order = orderService.create(customerOrder);
        return Response.ok(order).build();
    }

    @DELETE
    @Path("/cancel/{customerId}")
    @Produces(APPLICATION_JSON)
    public Response cancel(@PathParam("customerId") String customerId) {
        boolean found = orderService.cancel(customerId);
        if (!found) {
            throw new WebApplicationException(status(NOT_FOUND)
                                            .entity(new ErrorBean("Müşteri sepeti mevcut değil. Lütfen yeni bir sepet oluşturunuz."))
                                            .build());
        }
        Map<String, String> map = new LinkedHashMap<>();
        map.put("message", "sipariş iptal edildi");
        return Response.ok(map).build();
    }

    @GET
    @Path("/{customerId}")
    @Produces(APPLICATION_JSON)
    public OrderBean get(@PathParam("customerId") String customerId) {
        OrderBean order = orderService.getByCustomerId(customerId);
        if (order == null) {
            throw new WebApplicationException(status(NOT_FOUND)
                                              .entity(new ErrorBean("Müşteri sepeti mevcut değil."))
                                              .build());
        }
        return order;
    }

    @PUT
    @Path("/product/{customerId}/{productId}")
    @Produces(APPLICATION_JSON)
    public OrderBean put(@PathParam("customerId") String customerId,
                         @PathParam("productId") String productId) {
        orderService.createOrderDetail(customerId, productId);
        return get(customerId);
    }

    @DELETE
    @Produces(APPLICATION_JSON)
    @Path("/product/{customerId}/{productId}")
    public OrderBean delete(@PathParam("customerId") String customerId,
                            @PathParam("productId") String productId) {
        orderService.delete(customerId, productId);
        return get(customerId);
    }
}
