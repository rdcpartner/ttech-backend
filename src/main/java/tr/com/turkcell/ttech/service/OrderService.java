package tr.com.turkcell.ttech.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.turkcell.ttech.domain.OrderBean;
import tr.com.turkcell.ttech.domain.OrderBean.Builder;
import tr.com.turkcell.ttech.repository.CustomerOrderDetailRepository;
import tr.com.turkcell.ttech.repository.OrderRepository;
import tr.com.turkcell.ttech.tables.pojos.CustomerOrder;
import tr.com.turkcell.ttech.tables.pojos.CustomerOrderDetail;
import tr.com.turkcell.ttech.tables.pojos.Otp;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerOrderDetailRepository orderDetailRepository;

    @Autowired
    private OtpService otpService;

    public CustomerOrder create(CustomerOrder order) {
        return orderRepository.create(order);
    }

    public OrderBean getByCustomerId(String customerId) {
        CustomerOrder customerOrder = orderRepository.getLatestOrder(customerId);
        if (customerOrder == null) {
            return null;
        }
        Builder builder = OrderBean.builder();
        builder.withCustomerId(customerOrder.getCustomerId());
        List<CustomerOrderDetail> orders = orderDetailRepository.list(customerOrder.getId());
        List<String> products = new ArrayList<>(orders.size());
        for (CustomerOrderDetail next : orders) {
            products.add(next.getProductId());
        }
        String otpId = customerOrder.getOtpId();
        if (otpId != null) {
            Otp otp = otpService.get(otpId);
            if (otp != null) {
                builder.withVerified(Boolean.TRUE.equals(otp.getVerified()));
            }
        }
        builder.withOrderId(customerOrder.getId());
        builder.withProducts(products);
        return builder.build();
    }

    public List<CustomerOrder> list() {
        return orderRepository.list();
    }

    public OrderBean delete(String customerId, String productId) {
        OrderBean order = getByCustomerId(customerId);
        String orderId = order.getOrderId();
        orderDetailRepository.delete(orderId, productId);
        return getByCustomerId(customerId);
    }

    public void createOrderDetail(String customerId, String productId) {
        CustomerOrderDetail orderDetail = new CustomerOrderDetail();
        orderDetail.setProductId(productId);
        OrderBean order = getByCustomerId(customerId);
        orderDetail.setOrderId(order.getOrderId());
        orderDetailRepository.create(orderDetail);
    }

    public boolean cancel(String customerId) {
        OrderBean order = getByCustomerId(customerId);
        if (order == null) {
            return false;
        }
        for (String next : order.getProducts()) {
            delete(customerId, next);
        }
        orderRepository.delete(customerId);
        return true;
    }
}
