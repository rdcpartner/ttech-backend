package tr.com.turkcell.ttech.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.turkcell.ttech.repository.ProductRepository;
import tr.com.turkcell.ttech.tables.pojos.Product;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    
    public Product create(Product product) {
        return productRepository.create(product);
    }

    public Product update(Product product) {
        return productRepository.update(product);
    }

    public List<Product> list() {
        return productRepository.list();
    }

    public Product get(String id) {
        return productRepository.get(id);
    }
}
