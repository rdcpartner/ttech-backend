package tr.com.turkcell.ttech.service;

import static java.time.LocalDateTime.now;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.turkcell.ttech.repository.CustomerOrderDetailRepository;
import tr.com.turkcell.ttech.repository.CustomerRepository;
import tr.com.turkcell.ttech.tables.pojos.Customer;
import tr.com.turkcell.ttech.tables.pojos.CustomerOrderDetail;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerOrderDetailRepository customerOrderDetailRepository;

    public List<Customer> list() {
        return customerRepository.list();
    }

    public Customer get(String id) {
        try {
            return customerRepository.get(id);
        } catch (DataAccessException e) {
            return null;
        }
    }

    public void add(String orderId, List<String> productIds) {
        LocalDateTime now = now();
        for (String next : productIds) {
            CustomerOrderDetail orderDetail = new CustomerOrderDetail();
            orderDetail.setId(UUID.randomUUID().toString());
            orderDetail.setCreateDate(now);
            orderDetail.setUpdateDate(now);
            orderDetail.setOrderId(orderId);
            orderDetail.setProductId(next);
            customerOrderDetailRepository.create(orderDetail);
        }
    }

    public List<Customer> search(String msIsdn, String name, String surname) {
        return customerRepository.search(msIsdn, name, surname);
    }
}
