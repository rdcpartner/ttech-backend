package tr.com.turkcell.ttech.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tr.com.turkcell.ttech.repository.OrderRepository;
import tr.com.turkcell.ttech.repository.OtpRepository;
import tr.com.turkcell.ttech.tables.pojos.Otp;

@Service
public class OtpService {

    @Autowired
    private OtpRepository otpRepository;

    @Autowired
    private OrderRepository orderRepository;

    public String send(String customerId) {
        Otp otp = new Otp();
        otp.setMsisdn("5321111111");
        Otp created = otpRepository.create(otp);
        orderRepository.updateOtpId(customerId, created.getId());
        return created.getId();
    }

    public void verify(String otpId) {
        otpRepository.verify(otpId);
    }

    public Otp get(String otpId) {
        return otpRepository.get(otpId);
    }
}
