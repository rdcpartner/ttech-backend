package tr.com.turkcell.ttech.converter;

import static tr.com.turkcell.ttech.domain.PricingPeriod.Monthly;
import static tr.com.turkcell.ttech.domain.PricingPeriod.Yearly;

import org.jooq.Converter;

import tr.com.turkcell.ttech.domain.PricingPeriod;

public class PricingPeriodConverter implements Converter<String, PricingPeriod> {

    private static final long serialVersionUID = 1130799150832192707L;

    @Override
    public PricingPeriod from(String databaseObject) {
        if (Monthly.value.equals(databaseObject)) {
            return Monthly;
        } else if (Yearly.value.contentEquals(databaseObject)) {
            return Yearly;
        }
        return null;
    }

    @Override
    public String to(PricingPeriod userObject) {
        return userObject.value;
    }

    @Override
    public Class<String> fromType() {
        return String.class;
    }

    @Override
    public Class<PricingPeriod> toType() {
        return PricingPeriod.class;
    }
}
