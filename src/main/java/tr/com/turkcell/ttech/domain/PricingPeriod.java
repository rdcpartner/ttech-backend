package tr.com.turkcell.ttech.domain;

public enum PricingPeriod {
    Monthly("M"),
    Yearly("Y");

    public final String value;

    private PricingPeriod(String value) {
        this.value = value;
    }
}
