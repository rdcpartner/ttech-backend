package tr.com.turkcell.ttech.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OrderBean {

    private String customerId;

    private List<String> products = new ArrayList<>();

    private boolean verified;

    @JsonIgnore
    private String orderId;

    private OrderBean(Builder builder) {
        this.customerId = builder.customerId;
        this.products = builder.products;
        this.verified = builder.verified;
        this.orderId = builder.orderId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String customerId;
        private boolean verified;
        private String orderId;
        private List<String> products = Collections.emptyList();

        private Builder() {
        }


        public Builder withCustomerId(String customerId) {
            this.customerId = customerId;
            return this;
        }

        public Builder withProducts(List<String> products) {
            this.products = products;
            return this;
        }

        public Builder withVerified(boolean verified) {
            this.verified = verified;
            return this;
        }

        public Builder withOrderId(String orderId) {
            this.orderId = orderId;
            return this;
        }
        
        public OrderBean build() {
            return new OrderBean(this);
        }
    }

    public String getCustomerId() {
        return customerId;
    }

    public List<String> getProducts() {
        return products;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getOrderId() {
        return orderId;
    }
}
