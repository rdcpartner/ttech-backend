package tr.com.turkcell.ttech.domain;

public class ErrorBean {

    private final String message;

    public ErrorBean(String message) {
        this.message = message;
    }
  
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ErrorBean [message=" + message + "]";
    }
}
