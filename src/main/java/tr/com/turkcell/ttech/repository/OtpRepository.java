package tr.com.turkcell.ttech.repository;

import static tr.com.turkcell.ttech.Tables.OTP;

import java.time.LocalDateTime;
import java.util.UUID;

import org.jooq.DSLContext;
import org.jooq.exception.NoDataFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.com.turkcell.ttech.tables.pojos.Otp;
import tr.com.turkcell.ttech.tables.records.OtpRecord;

@Repository
public class OtpRepository {

    @Autowired
    private DSLContext dsl;

    public Otp create(Otp otp) {
        otp.setId(UUID.randomUUID().toString());
        otp.setCreateDate(LocalDateTime.now());
        otp.setUpdateDate(LocalDateTime.now());
        otp.setActive(Boolean.TRUE);
        OtpRecord record = dsl.newRecord(OTP, otp);
        record.insert();
        return get(otp.getId());
    }

    public Otp get(String id) {
        try {
        Otp record = dsl.select()
                            .from(OTP)
                            .where(OTP.ID.eq(id))
                            .fetchSingle()
                        .into(Otp.class);
        return record;
        } catch (NoDataFoundException e) {
            return null;
        }
    }

    public void verify(String id) {
        Otp otp = get(id);
        OtpRecord otpRecord = dsl.newRecord(OTP, otp);
        otpRecord.setVerified(Boolean.TRUE);
        dsl.update(OTP)
            .set(otpRecord)
            .where(OTP.ID.eq(id))
            .execute();
    }
}
