package tr.com.turkcell.ttech.repository;

import static tr.com.turkcell.ttech.Tables.CUSTOMER_ORDER;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.jooq.DSLContext;
import org.jooq.exception.NoDataFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.com.turkcell.ttech.tables.pojos.CustomerOrder;
import tr.com.turkcell.ttech.tables.records.CustomerOrderRecord;

@Repository
public class OrderRepository {

    @Autowired
    private DSLContext dsl;

    public CustomerOrder create(CustomerOrder order) {
        order.setId(UUID.randomUUID().toString());
        order.setCreateDate(LocalDateTime.now());
        order.setUpdateDate(LocalDateTime.now());
        order.setActive(Boolean.TRUE);
        CustomerOrderRecord record = dsl.newRecord(CUSTOMER_ORDER, order);
        record.insert();
        return getLatestOrder(order.getCustomerId());
    }

    public CustomerOrder getLatestOrder(String customerId) {
        try {
        CustomerOrder record = dsl.select()
                                    .from(CUSTOMER_ORDER)
                                    .where(CUSTOMER_ORDER.CUSTOMER_ID.eq(customerId)
                                            .and(CUSTOMER_ORDER.OTP_ID.isNull()))
                                    .orderBy(CUSTOMER_ORDER.CREATE_DATE.desc())
                                    .fetchSingle()
                                .into(CustomerOrder.class);
        return record;
        } catch (NoDataFoundException e) {
            return null;
        }
    }

    public void updateOtpId(String customerId, String otpId) {
        dsl.update(CUSTOMER_ORDER)
                .set(CUSTOMER_ORDER.OTP_ID, otpId)
                    .where(CUSTOMER_ORDER.CUSTOMER_ID.eq(customerId)
                            .and(CUSTOMER_ORDER.OTP_ID.isNull()))
                    .execute();
    }
    
    public void delete(String customerId) {
        dsl.delete(CUSTOMER_ORDER)
        .where(CUSTOMER_ORDER.CUSTOMER_ID.eq(customerId)
                    .and(CUSTOMER_ORDER.OTP_ID.isNull()))
        .execute();
    }
    
    public List<CustomerOrder> list() {
        List<CustomerOrder> list = dsl.select()
                                    .from(CUSTOMER_ORDER)
                                    .fetch()
                                .into(CustomerOrder.class);
        return list;
    }
}
