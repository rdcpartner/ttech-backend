package tr.com.turkcell.ttech.repository;

import static tr.com.turkcell.ttech.Tables.PRODUCT;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.com.turkcell.ttech.tables.pojos.Product;
import tr.com.turkcell.ttech.tables.records.ProductRecord;

@Repository
public class ProductRepository {

    @Autowired
    private DSLContext dsl;

    public Product create(Product product) {
        product.setCreateDate(LocalDateTime.now());
        product.setUpdateDate(LocalDateTime.now());
        product.setId(UUID.randomUUID().toString());
        ProductRecord record = dsl.newRecord(PRODUCT, product);
        record.insert();
        return get(product.getId());
    }

    public List<Product> list() {
        List<Product> list = dsl.select()
                                    .from(PRODUCT)
                                    .fetch()
                                .into(Product.class);
        return list;
    }

    public Product update(@Valid Product product) {
        ProductRecord record = dsl.newRecord(PRODUCT, product);
        dsl.update(PRODUCT).set(record)
                                .where(PRODUCT.ID.eq(product.getId()))
                            .execute();
        return get(product.getId());
    }

    public Product get(String id) {
        Product record = dsl.select()
                            .from(PRODUCT)
                            .where(PRODUCT.ID.eq(id))
                            .fetchSingle()
                        .into(Product.class);
        return record;
    }
}
