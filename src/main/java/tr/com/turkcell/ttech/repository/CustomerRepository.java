package tr.com.turkcell.ttech.repository;

import static tr.com.turkcell.ttech.Tables.CUSTOMER;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectJoinStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.com.turkcell.ttech.tables.pojos.Customer;
import tr.com.turkcell.ttech.tables.records.CustomerRecord;

@Repository
public class CustomerRepository {

    @Autowired
    private DSLContext dsl;

    public Customer create(Customer customer) {
        customer.setId(UUID.randomUUID().toString());
        customer.setCreateDate(LocalDateTime.now());
        customer.setUpdateDate(LocalDateTime.now());
        CustomerRecord record = dsl.newRecord(CUSTOMER, customer);
        record.insert();
        return get(customer.getId());
    }

    public List<Customer> list() {
        List<Customer> list = dsl.select()
                                    .from(CUSTOMER)
                                    .fetch()
                                .into(Customer.class);
        return list;
    }

    public Customer get(String id) {
        Customer record = dsl.select()
                            .from(CUSTOMER)
                            .where(CUSTOMER.ID.eq(id))
                            .fetchSingle()
                        .into(Customer.class);
        return record;
    }

    public List<Customer> search(String msIsdn, String name, String surname) {
        SelectJoinStep<Record> query = dsl.select()
                                            .from(CUSTOMER);
        if (msIsdn != null && !msIsdn.trim().isEmpty()) {
            query.where(CUSTOMER.MSISDN.likeIgnoreCase(msIsdn + "%"));
        }
        if (name != null && !name.trim().isEmpty()) {
            query.where(CUSTOMER.NAME.likeIgnoreCase(name + "%"));
        }
        if (surname != null && !surname.trim().isEmpty()) {
            query.where(CUSTOMER.SURNAME.likeIgnoreCase(surname + "%"));
        }
        return query.fetch().into(Customer.class);
    }
}
