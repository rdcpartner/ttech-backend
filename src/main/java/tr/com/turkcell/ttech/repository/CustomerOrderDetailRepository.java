package tr.com.turkcell.ttech.repository;

import static tr.com.turkcell.ttech.Tables.CUSTOMER_ORDER_DETAIL;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import tr.com.turkcell.ttech.tables.pojos.CustomerOrderDetail;
import tr.com.turkcell.ttech.tables.records.CustomerOrderDetailRecord;

@Repository
public class CustomerOrderDetailRepository {

    @Autowired
    private DSLContext dsl;

    public CustomerOrderDetail create(CustomerOrderDetail orderDetail) {
        orderDetail.setId(UUID.randomUUID().toString());
        orderDetail.setCreateDate(LocalDateTime.now());
        orderDetail.setUpdateDate(LocalDateTime.now());
        orderDetail.setActive(Boolean.TRUE);
        CustomerOrderDetailRecord record = dsl.newRecord(CUSTOMER_ORDER_DETAIL, orderDetail);
        record.insert();
        return get(orderDetail.getId());
    }

    public CustomerOrderDetail get(String id) {
        CustomerOrderDetail record = dsl.select()
                                        .from(CUSTOMER_ORDER_DETAIL)
                                        .where(CUSTOMER_ORDER_DETAIL.ID.eq(id))
                                        .fetchSingle()
                                    .into(CustomerOrderDetail.class);
        return record;
    }

    public List<CustomerOrderDetail> list(String orderId) {
        try {
            List<CustomerOrderDetail> list = dsl.select()
                                                    .from(CUSTOMER_ORDER_DETAIL)
                                                    .where(CUSTOMER_ORDER_DETAIL.ORDER_ID.eq(orderId))
                                                    .fetch()
                                                .into(CustomerOrderDetail.class);
            return list;
        } catch (DataAccessException e) {
            return Collections.emptyList();
        }
    }

    public void delete(String orderId, String productId) {
        dsl.delete(CUSTOMER_ORDER_DETAIL)
            .where(CUSTOMER_ORDER_DETAIL.PRODUCT_ID.eq(productId)
                    .and(CUSTOMER_ORDER_DETAIL.ORDER_ID.eq(orderId)))
            .execute();
    }
}
