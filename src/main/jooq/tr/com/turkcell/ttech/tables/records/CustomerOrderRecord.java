/*
 * This file is generated by jOOQ.
 */
package tr.com.turkcell.ttech.tables.records;


import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;

import tr.com.turkcell.ttech.tables.CustomerOrder;
import tr.com.turkcell.ttech.tables.interfaces.ICustomerOrder;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CustomerOrderRecord extends UpdatableRecordImpl<CustomerOrderRecord> implements Record6<String, String, LocalDateTime, LocalDateTime, String, Boolean>, ICustomerOrder {

    private static final long serialVersionUID = -1827809585;

    /**
     * Setter for <code>TTECH.CUSTOMER_ORDER.ID</code>.
     */
    @Override
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>TTECH.CUSTOMER_ORDER.ID</code>.
     */
    @NotNull
    @Size(max = 36)
    @Override
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>TTECH.CUSTOMER_ORDER.CUSTOMER_ID</code>.
     */
    @Override
    public void setCustomerId(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>TTECH.CUSTOMER_ORDER.CUSTOMER_ID</code>.
     */
    @NotNull
    @Size(max = 36)
    @Override
    public String getCustomerId() {
        return (String) get(1);
    }

    /**
     * Setter for <code>TTECH.CUSTOMER_ORDER.CREATE_DATE</code>.
     */
    @Override
    public void setCreateDate(LocalDateTime value) {
        set(2, value);
    }

    /**
     * Getter for <code>TTECH.CUSTOMER_ORDER.CREATE_DATE</code>.
     */
    @NotNull
    @Override
    public LocalDateTime getCreateDate() {
        return (LocalDateTime) get(2);
    }

    /**
     * Setter for <code>TTECH.CUSTOMER_ORDER.UPDATE_DATE</code>.
     */
    @Override
    public void setUpdateDate(LocalDateTime value) {
        set(3, value);
    }

    /**
     * Getter for <code>TTECH.CUSTOMER_ORDER.UPDATE_DATE</code>.
     */
    @NotNull
    @Override
    public LocalDateTime getUpdateDate() {
        return (LocalDateTime) get(3);
    }

    /**
     * Setter for <code>TTECH.CUSTOMER_ORDER.OTP_ID</code>.
     */
    @Override
    public void setOtpId(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>TTECH.CUSTOMER_ORDER.OTP_ID</code>.
     */
    @Size(max = 36)
    @Override
    public String getOtpId() {
        return (String) get(4);
    }

    /**
     * Setter for <code>TTECH.CUSTOMER_ORDER.ACTIVE</code>.
     */
    @Override
    public void setActive(Boolean value) {
        set(5, value);
    }

    /**
     * Getter for <code>TTECH.CUSTOMER_ORDER.ACTIVE</code>.
     */
    @Override
    public Boolean getActive() {
        return (Boolean) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row6<String, String, LocalDateTime, LocalDateTime, String, Boolean> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    @Override
    public Row6<String, String, LocalDateTime, LocalDateTime, String, Boolean> valuesRow() {
        return (Row6) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return CustomerOrder.CUSTOMER_ORDER.ID;
    }

    @Override
    public Field<String> field2() {
        return CustomerOrder.CUSTOMER_ORDER.CUSTOMER_ID;
    }

    @Override
    public Field<LocalDateTime> field3() {
        return CustomerOrder.CUSTOMER_ORDER.CREATE_DATE;
    }

    @Override
    public Field<LocalDateTime> field4() {
        return CustomerOrder.CUSTOMER_ORDER.UPDATE_DATE;
    }

    @Override
    public Field<String> field5() {
        return CustomerOrder.CUSTOMER_ORDER.OTP_ID;
    }

    @Override
    public Field<Boolean> field6() {
        return CustomerOrder.CUSTOMER_ORDER.ACTIVE;
    }

    @Override
    public String component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getCustomerId();
    }

    @Override
    public LocalDateTime component3() {
        return getCreateDate();
    }

    @Override
    public LocalDateTime component4() {
        return getUpdateDate();
    }

    @Override
    public String component5() {
        return getOtpId();
    }

    @Override
    public Boolean component6() {
        return getActive();
    }

    @Override
    public String value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getCustomerId();
    }

    @Override
    public LocalDateTime value3() {
        return getCreateDate();
    }

    @Override
    public LocalDateTime value4() {
        return getUpdateDate();
    }

    @Override
    public String value5() {
        return getOtpId();
    }

    @Override
    public Boolean value6() {
        return getActive();
    }

    @Override
    public CustomerOrderRecord value1(String value) {
        setId(value);
        return this;
    }

    @Override
    public CustomerOrderRecord value2(String value) {
        setCustomerId(value);
        return this;
    }

    @Override
    public CustomerOrderRecord value3(LocalDateTime value) {
        setCreateDate(value);
        return this;
    }

    @Override
    public CustomerOrderRecord value4(LocalDateTime value) {
        setUpdateDate(value);
        return this;
    }

    @Override
    public CustomerOrderRecord value5(String value) {
        setOtpId(value);
        return this;
    }

    @Override
    public CustomerOrderRecord value6(Boolean value) {
        setActive(value);
        return this;
    }

    @Override
    public CustomerOrderRecord values(String value1, String value2, LocalDateTime value3, LocalDateTime value4, String value5, Boolean value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // FROM and INTO
    // -------------------------------------------------------------------------

    @Override
    public void from(ICustomerOrder from) {
        setId(from.getId());
        setCustomerId(from.getCustomerId());
        setCreateDate(from.getCreateDate());
        setUpdateDate(from.getUpdateDate());
        setOtpId(from.getOtpId());
        setActive(from.getActive());
    }

    @Override
    public <E extends ICustomerOrder> E into(E into) {
        into.from(this);
        return into;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached CustomerOrderRecord
     */
    public CustomerOrderRecord() {
        super(CustomerOrder.CUSTOMER_ORDER);
    }

    /**
     * Create a detached, initialised CustomerOrderRecord
     */
    public CustomerOrderRecord(String id, String customerId, LocalDateTime createDate, LocalDateTime updateDate, String otpId, Boolean active) {
        super(CustomerOrder.CUSTOMER_ORDER);

        set(0, id);
        set(1, customerId);
        set(2, createDate);
        set(3, updateDate);
        set(4, otpId);
        set(5, active);
    }
}
