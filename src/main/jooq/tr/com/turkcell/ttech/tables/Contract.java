/*
 * This file is generated by jOOQ.
 */
package tr.com.turkcell.ttech.tables;


import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row6;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import tr.com.turkcell.ttech.Indexes;
import tr.com.turkcell.ttech.Keys;
import tr.com.turkcell.ttech.Ttech;
import tr.com.turkcell.ttech.tables.records.ContractRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Contract extends TableImpl<ContractRecord> {

    private static final long serialVersionUID = 134800340;

    /**
     * The reference instance of <code>TTECH.CONTRACT</code>
     */
    public static final Contract CONTRACT = new Contract();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ContractRecord> getRecordType() {
        return ContractRecord.class;
    }

    /**
     * The column <code>TTECH.CONTRACT.ID</code>.
     */
    public final TableField<ContractRecord, String> ID = createField(DSL.name("ID"), org.jooq.impl.SQLDataType.CHAR(36).nullable(false), this, "");

    /**
     * The column <code>TTECH.CONTRACT.PRODUCT_ID</code>.
     */
    public final TableField<ContractRecord, String> PRODUCT_ID = createField(DSL.name("PRODUCT_ID"), org.jooq.impl.SQLDataType.CHAR(36).nullable(false), this, "");

    /**
     * The column <code>TTECH.CONTRACT.CREATE_DATE</code>.
     */
    public final TableField<ContractRecord, LocalDateTime> CREATE_DATE = createField(DSL.name("CREATE_DATE"), org.jooq.impl.SQLDataType.LOCALDATETIME.nullable(false), this, "");

    /**
     * The column <code>TTECH.CONTRACT.UPDATE_DATE</code>.
     */
    public final TableField<ContractRecord, LocalDateTime> UPDATE_DATE = createField(DSL.name("UPDATE_DATE"), org.jooq.impl.SQLDataType.LOCALDATETIME.nullable(false), this, "");

    /**
     * The column <code>TTECH.CONTRACT.CONTENT</code>.
     */
    public final TableField<ContractRecord, byte[]> CONTENT = createField(DSL.name("CONTENT"), org.jooq.impl.SQLDataType.BLOB, this, "");

    /**
     * The column <code>TTECH.CONTRACT.ACTIVE</code>.
     */
    public final TableField<ContractRecord, Boolean> ACTIVE = createField(DSL.name("ACTIVE"), org.jooq.impl.SQLDataType.BOOLEAN.defaultValue(org.jooq.impl.DSL.field("'1'", org.jooq.impl.SQLDataType.BOOLEAN)), this, "");

    /**
     * Create a <code>TTECH.CONTRACT</code> table reference
     */
    public Contract() {
        this(DSL.name("CONTRACT"), null);
    }

    /**
     * Create an aliased <code>TTECH.CONTRACT</code> table reference
     */
    public Contract(String alias) {
        this(DSL.name(alias), CONTRACT);
    }

    /**
     * Create an aliased <code>TTECH.CONTRACT</code> table reference
     */
    public Contract(Name alias) {
        this(alias, CONTRACT);
    }

    private Contract(Name alias, Table<ContractRecord> aliased) {
        this(alias, aliased, null);
    }

    private Contract(Name alias, Table<ContractRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Contract(Table<O> child, ForeignKey<O, ContractRecord> key) {
        super(child, key, CONTRACT);
    }

    @Override
    public Schema getSchema() {
        return Ttech.TTECH;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.PRIMARY_KEY_C);
    }

    @Override
    public UniqueKey<ContractRecord> getPrimaryKey() {
        return Keys.CONSTRAINT_C;
    }

    @Override
    public List<UniqueKey<ContractRecord>> getKeys() {
        return Arrays.<UniqueKey<ContractRecord>>asList(Keys.CONSTRAINT_C);
    }

    @Override
    public Contract as(String alias) {
        return new Contract(DSL.name(alias), this);
    }

    @Override
    public Contract as(Name alias) {
        return new Contract(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Contract rename(String name) {
        return new Contract(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Contract rename(Name name) {
        return new Contract(name, null);
    }

    // -------------------------------------------------------------------------
    // Row6 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row6<String, String, LocalDateTime, LocalDateTime, byte[], Boolean> fieldsRow() {
        return (Row6) super.fieldsRow();
    }
}
