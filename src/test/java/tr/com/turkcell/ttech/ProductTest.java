package tr.com.turkcell.ttech;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import tr.com.turkcell.ttech.domain.PricingPeriod;
import tr.com.turkcell.ttech.tables.pojos.Product;

@SpringBootTest(webEnvironment =  RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ProductTest {
    
    @LocalServerPort
    private int port;

    @BeforeEach
    public void before() {
        baseURI = "http://localhost:" + port;
    }

	@Test
	@Order(1)
	public void testInsert() {
	    Product product = new Product();
	    product.setName("fizzy");
	    product.setActive(Boolean.TRUE);
	    product.setPrice(new BigDecimal("19.99"));
	    product.setPricePeriod(PricingPeriod.Monthly);
	    Product inserted = given()
                                .contentType(JSON)
                                .accept(JSON)
                                .body(product)
                                .post("/product")
                                .then()
                                .statusCode(200)
                                .extract()
                                .as(Product.class);
	    assertNotNull(inserted);
        assertNotNull(inserted.getId());
        assertEquals("fizzy", product.getName());
	}
}
