package tr.com.turkcell.ttech;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import io.restassured.mapper.TypeRef;
import tr.com.turkcell.ttech.repository.CustomerRepository;
import tr.com.turkcell.ttech.tables.pojos.Customer;

@SpringBootTest(webEnvironment =  RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CustomerTest {
    
    private final TypeRef<List<Customer>> customerTypeRef = new TypeRef<List<Customer>>() {};

    @Autowired
    private CustomerRepository customerService;

    @LocalServerPort
    private int port;

    @BeforeEach
    public void before() {
        baseURI = "http://localhost:" + port;
    }

	@Test
	@Order(1)
	public void testInsert() {
	    Customer customer = new Customer();
	    customer.setMsisdn("5321111111");
	    customer.setName("joe");
	    customer.setSurname("doe");
        customerService.create(customer);

        List<Customer> customers = customerService.list();
        assertTrue(customers.size() > 0);
        Customer inserted = customers.get(0);
        assertEquals(inserted.getName(), customer.getName());
        assertEquals(inserted.getSurname(), customer.getSurname());
	}

	@Test
	@Order(2)
	public void testList() {
        List<Customer> customers = given()
                                        .get("/customer")
                                        .then()
                                        .statusCode(200)
                                        .extract()
                                        .response()
                                    .as(customerTypeRef);
        assertTrue(customers.size() > 0);
	}
}
